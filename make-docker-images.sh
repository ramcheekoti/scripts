#!/bin/bash
# TO make docker image first we need to check whether dockerfile exists or not
if [ ! -f Dockerfile ]  #(This logic if docker file does not exists)
then

   echo -e "Script did not find any Dockerfile in local directory"
   exit 1

fi


IMAGE=$(head -1 Dockerfile | grep "^#" | sed -e 's/#//')
# to check if docker file is empty 

if [ -z $IMAGE ]  # -z checks if variable is empty
then

   echo "Image name not found in Docker file, can not proceed"
   exit 1
fi


# Now I want to make an image now

docker build -t $IMAGE .
# the below logic if build fails
if [ $? -ne 0 ]
then
    
    echo -e "BUILD FAILURE"
    exit 1
fi

# the below logic while passing first argument

case $1 in
  -p*|push)
     docker push $IMAGE
     ;;
esac


    



